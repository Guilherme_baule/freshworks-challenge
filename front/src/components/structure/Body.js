import React from 'react';
import "@material-tailwind/react/tailwind.css";
import IndexNav from './IndexNav'
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";
import ListParks from '../../pages/list/ListParks';
import FeedInterface from '../../pages/feed/FeedInterface';
import CreatePark from '../../pages/create/CreatePark';

export default class Body extends React.Component {

    render() {
        return (
            <Router>
                <IndexNav/>
                <div class="flex">
                    <div class="flex-none w-16 h-16"></div>
                        <div class="flex-grow h-16">
                            <Switch>
                            <Route path="/create">
                                    <CreatePark/>
                                </Route>
                                <Route path="/feed/:name">
                                    <FeedInterface/>
                                </Route>
                                <Route path="/">
                                    <ListParks/>
                                </Route>
                            </Switch>
                        </div>
                    <div class="flex-none w-16 h-16"></div>
                </div>
            </Router>
        )
    }
}

