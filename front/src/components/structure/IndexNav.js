import React, { useState } from "react";
import Navbar from "@material-tailwind/react/Navbar";
import NavbarContainer from "@material-tailwind/react/NavbarContainer";
import NavbarWrapper from "@material-tailwind/react/NavbarWrapper";
import NavbarBrand from "@material-tailwind/react/NavbarBrand";
import NavbarToggler from "@material-tailwind/react/NavbarToggler";
import NavbarCollapse from "@material-tailwind/react/NavbarCollapse";
import Nav from "@material-tailwind/react/Nav";
import NavLink from "@material-tailwind/react/NavLink";

export default function IndexNav() {
  const [openNavbar, setOpenNavbar] = useState(false);

  
  return (
    <Navbar color="orange" navbar>
        <NavbarContainer>
        <NavbarWrapper>
                <NavbarBrand>Freshworks</NavbarBrand>
                <NavbarToggler
                    color="white"
                    onClick={() => setOpenNavbar(!openNavbar)}
                    ripple="light"
                />
            </NavbarWrapper>
            <NavbarCollapse open={openNavbar}>
                <Nav leftSide>
                    <NavLink href="/" ripple="light">
                        List Parks
                    </NavLink>
                    <NavLink href="/create" ripple="light">
                        Create Park
                    </NavLink>
                </Nav>
            </NavbarCollapse>
        </NavbarContainer>
    </Navbar>
  );
}