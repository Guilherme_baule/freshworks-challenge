import React from "react";
import Input from "@material-tailwind/react/Input";
import Button from "@material-tailwind/react/Button";
import { Redirect } from 'react-router-dom'
import Alert from "@material-tailwind/react/Alert";

export default class FeedForm extends React.Component {

    state = {
        foodTypeProp: {},
        foodQuantityProp: {},
        foodNameProp: {},
        quantityProp: {},
        fedQuantityProp: {},
        httpStatus: {
            active: false,
            error: false,
            message: ""
        }
    }

    createMessage() {
        let http = this.state.httpStatus
        return (
            <Alert color={http.error ? "red" : "green"}>{http.message}</Alert>
        )
    }

    handleInputChange = (event) => {
        const target = event.target;
        let value = target.value;
        const name = target.name;
        const prop = `${name}Prop`

        if (target.type === "text" && value.length < 3) {
            this.setState({[prop]: {
                error: "Minimal of 3 characteres"
            }})
        } else if (target.type === "number" && value === "") {
            value = 0
        } else {
            this.setState({[prop]: {}})
        }
    
        this.setState({
          [name]: value
        });
    }

    submit = (e) => {
        e.preventDefault();

        if (this.state.foodType === undefined || this.state.foodType.length < 3) return false
        if (this.state.foodName === undefined || this.state.foodName.length < 3) return false
        if (this.state.foodQuantity === undefined) return false
        if (this.state.quantity === undefined) return false
        if (this.state.fedQuantity === undefined) return false

        this.props.submit({
            "food": {
                "type": this.state.foodType,
                "name": this.state.foodName,
                "quantity": this.state.foodQuantity
            },
            "quantity": this.state.quantity,
            "fedQuantity": this.state.fedQuantity
        })
        .then(() => {
            this.setState({redirect: true})
        })
        .catch(e => {
            this.setState({httpStatus: {
                active: true,
                error: true,
                message: e.error
            }})
        })
    }

    render() {
        if (this.state.redirect === true) return (<Redirect to="/" />)

        return (
            <div className="flex justify-center py-4 lg:pt-4 pt-8">
                <div className="mr-4 p-3">
                    <Input
                        type="text"
                        color="lightBlue"
                        size="regular"
                        outline={false}
                        placeholder="Food Type"
                        name="foodType"
                        onChange={this.handleInputChange}
                        {...this.state.foodTypeProp}
                    />
                </div>
                <div className="mr-10 p-2">
                    <Input
                        type="number"
                        color="lightBlue"
                        size="regular"
                        outline={false}
                        placeholder="Food Quantity Per Duck"
                        name="foodQuantity"
                        onChange={this.handleInputChange}
                        {...this.state.foodQuantityProp}
                    />
                </div>
                <div className="mr-10 p-2">
                    <Input
                        type="text"
                        color="lightBlue"
                        size="regular"
                        outline={false}
                        placeholder="Food Name"
                        name="foodName"
                        onChange={this.handleInputChange}
                        {...this.state.foodNameProp}
                    />
                </div>
                <div className="mr-10 p-2">
                    <Input
                        type="number"
                        color="lightBlue"
                        size="regular"
                        outline={false}
                        placeholder="Ducks quantity"
                        name="quantity"
                        onChange={this.handleInputChange}
                        {...this.state.quantityProp}
                    />
                </div>
                <div className="mr-10 p-2">
                    <Input
                        type="number"
                        color="lightBlue"
                        size="regular"
                        outline={false}
                        placeholder="Fed Ducks quantity"
                        name="fedQuantity"
                        onChange={this.handleInputChange}
                        {...this.state.fedQuantityProp}
                    />
                </div>
                <div className="mr-10 p-2">
                    <Button
                        onClick={this.submit}
                        rounded={true}
                        color="orange"
                        buttonType="outline"
                        size="lg"
                        ripple="dark">
                        Submit
                    </Button>
                </div>

                {this.state.httpStatus.active ? this.createMessage() : ""}
            </div>
        )
    }
}
