import React from "react";
import Card from "@material-tailwind/react/Card";
import CardHeader from "@material-tailwind/react/CardHeader";
import CardBody from "@material-tailwind/react/CardBody";
import Alert from "@material-tailwind/react/Alert";
import CardFooter from "@material-tailwind/react/CardFooter";
import InputIcon from "@material-tailwind/react/InputIcon";
import Button from "@material-tailwind/react/Button";
import H5 from "@material-tailwind/react/Heading5";
import LoginRepository from "../resources/login/LoginRepository";

export default class LoginCard extends React.Component {

    repository = new LoginRepository()

    state = {
        isRegistering: false,
        usernameProps: {},
        passwordProps: {},
        httpStatus: {
            active: false,
            error: false,
            message: ""
        }
    }

    createMessage() {
        let http = this.state.httpStatus
        return (
            <CardFooter>
                    <Alert color={http.error ? "red" : "green"}>{http.message}</Alert>
            </CardFooter>
        )
    }

    switch = (e) => {
        e.preventDefault();
        this.setState({isRegistering: !this.state.isRegistering})
    }

    submit = (e) => {
        e.preventDefault();

        if (this.state.username === undefined || this.state.username.length < 6) return false
        if (this.state.password === undefined || this.state.password.length < 6) return false

        if (this.state.isRegistering) {
            this.repository.register(this.state.username, this.state.password).then(() => {
                this.setState({
                    isRegistering: false,
                    httpStatus: {
                    active: true,
                    error: false,
                    message: "User created successfully"
                }})
            }).catch(e => {
                this.setState({httpStatus: {
                    active: true,
                    error: true,
                    message: e.error
                }})
            })
        } else {
            this.repository.login(this.state.username, this.state.password).then(() => {this.props.listener()}).catch(e => {
                this.setState({httpStatus: {
                    active: true,
                    error: true,
                    message: e.error
                }})
            })
        }
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        const prop = `${name}Props`

        if (value.length < 6) {
            this.setState({[prop]: {
                error: "Minimal of 6 characteres"
            }})
        } else {
            this.setState({[prop]: {}})
        }
    
        this.setState({
          [name]: value
        });
      }

    render() {
        let headText = this.state.isRegistering === true ? "Register" : "Login"
        let firstButton = this.state.isRegistering === true ? "Login" : "Register"
        let secondButton = headText

        return (
            <Card>
                <CardHeader color="amber" size="lg">
                    <H5 color="white">{headText}</H5>
                </CardHeader>

                <CardBody>
                    <div className="mb-8 px-4">
                        <InputIcon
                            onChange={this.handleInputChange}
                            name="username"
                            type="text"
                            color="lightBlue"
                            placeholder="Username"
                            iconName="account_circle"
                            {...this.state.usernameProps}
                        />
                    </div>
                    <div className="mb-4 px-4">
                        <InputIcon
                            onChange={this.handleInputChange}
                            name="password"
                            type="password"
                            color="lightBlue"
                            placeholder="password"
                            iconName="lock"
                            {...this.state.passwordProps}
                        />
                    </div>
                </CardBody>
                <CardFooter>
                        <div className="flex justify-between">
                            <Button
                                onClick={this.switch}
                                rounded={true}
                                color="orange"
                                buttonType="outline"
                                size="lg"
                                ripple="dark"
                            >
                                {firstButton}
                            </Button>

                            <Button
                                onClick={this.submit}
                                rounded={true}
                                color="deepOrange"
                                buttonType="filled"
                                size="lg"
                                ripple="dark"
                            >
                                {secondButton}
                            </Button>
                        </div>
                </CardFooter>
                {this.state.httpStatus.active ? this.createMessage() : ""}
            </Card>
        );
    }
}