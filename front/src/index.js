import { render } from "react-dom";
import Body from "./components/structure/Body";
import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";
import Login from "./pages/login/Login";

class Index extends React.Component {
    state = {
        logged: localStorage.getItem('token') !== null
    }

    recheckLoggedStatus() {
        this.setState({logged: localStorage.getItem('token') !== null})
    }

    render() {
        return (
        <Router>
            <Switch>
                <Route path="/">
                    { localStorage.getItem('token') !== null ? <Body handler = {this.recheckLoggedStatus}/> : <Login handler = {this.recheckLoggedStatus}/> }
                </Route>
            </Switch>
        </Router>
        )
    }
}


render(
    <Index/>,
    document.getElementById('root')
)