export async function authPostRequest(path, body) {
    return callWithBody(path, body, 'POST')
}

export async function authPutRequest(path, body) {
    return callWithBody(path, body, 'PUT')
}

export async function authGetRequest(path) {
    return callWithoutBody(path, 'GET')
}

async function callWithBody(path, body, method) {
    const request = await fetch(`${process.env.REACT_APP_SERVER_ENDPOINT}${path}`, {
        method: method,
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(body)
    })

    return handleResponse(request)
}

async function callWithoutBody(path, method) {
    const request = await fetch(`${process.env.REACT_APP_SERVER_ENDPOINT}${path}`, {
        method: method,
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer'
    })

    return handleResponse(request)
}

async function handleResponse(request) {
    const response = await request.text()

    if (request.status === 401) {
        localStorage.removeItem('token')
        window.location.href = '/';
    }

    if (request.status < 400) {
        if (response !== "") return JSON.parse(response)
        else return response
    }
    else throw response
}
