import { authPostRequest, authGetRequest, authPutRequest } from "../helper/FetchHelper"

export default class ParkRepository {

    async getAll() {
        return await authGetRequest("/")
    }

    async getById(id) {
        return await authGetRequest(`/${id}/history`)
    }

    async create(body) {
        return await authPostRequest('/create-park', body)
    }

    async feed(parkName, body) {
        return await authPutRequest(`/feed/${parkName}`, body)
    }

    async feedScheduler(parkName, body) {
        return await authPutRequest(`/feed/${parkName}/schedule`, body)
    }
}
