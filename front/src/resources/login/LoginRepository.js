export default class LoginRepository {

    async login(username, password) {
        const request = await fetch(`${process.env.REACT_APP_SERVER_ENDPOINT}/user/login`, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify({username, password})
        })

        let response = await request.json()

        if (request.status === 200) localStorage.setItem('token', response.token)
        else throw await request.json()

    }

    async register(username, password) {
            const request = await fetch(`${process.env.REACT_APP_SERVER_ENDPOINT}/user/register`, {
                method: 'POST',
                mode: 'cors',
                cache: 'no-cache',
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json'
                },
                redirect: 'follow',
                referrerPolicy: 'no-referrer',
                body: JSON.stringify({username, password})
        });
        
        if (request.status === 204) return true
        else throw await request.json()
    }
}
