import React from "react";
import FeedForm from "../../components/FeedForm";
import ParkRepository from "../../resources/park/ParkRepository";

export default class ListParks extends React.Component {

    parkRepository = new ParkRepository()

    submit = body => {
        return this.parkRepository.feed(this.props.parkName, body)
    }

    render() {
        return (<FeedForm submit={this.submit}/>)
    }
}