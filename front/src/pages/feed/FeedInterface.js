import { useParams } from "react-router";
import Feed from './Feed'

export default function FeedInterface() {
    const { name } = useParams()

    return (<Feed parkName={name}/>)
}