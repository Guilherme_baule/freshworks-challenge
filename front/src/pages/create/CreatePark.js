import Input from "@material-tailwind/react/Input";
import React from "react";
import { Redirect } from "react-router";
import FeedForm from "../../components/FeedForm";
import ParkRepository from "../../resources/park/ParkRepository";

export default class CreatePark extends React.Component {

    parkRepository = new ParkRepository()

    state = {
        parkNameProp: {},
        httpStatus: {
            active: false,
            error: false,
            message: ""
        }
    }

    handleInputChange = (event) => {
        const target = event.target;
        let value = target.value;
        const name = target.name;
        const prop = `${name}Prop`

        if (value.length < 3) {
            this.setState({[prop]: {
                error: "Minimal of 3 characteres"
            }})
        } else {
            this.setState({[prop]: {}})
        }
    
        this.setState({
          [name]: value
        });
    }

    submit = ducks => {
        if (this.state.parkName === undefined || this.state.parkName.length < 3) return false

        return this.parkRepository.create({
            name: this.state.parkName,
            ducks
        })
    }

    render() {
        if (this.state.redirect === true) return (<Redirect to="/" />)

        return (
            <div>
                <Input
                    type="text"
                    color="lightBlue"
                    size="regular"
                    outline={false}
                    placeholder="Park Name"
                    name="parkName"
                    onChange={this.handleInputChange}
                    {...this.state.parkNameProp}
                />
                <FeedForm submit={this.submit}/>
            </div>
        )
    }
}