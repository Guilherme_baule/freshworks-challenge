import React from "react";
import LoginCard from "../../components/LoginCard";

export default class Login extends React.Component {
    render() {
        return (
            <div class="w-screen h-screen relative flex flex-col justify-between bg-gray-500" color="gray">
                <div class="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2">
                    <LoginCard listener={this.props.handler}/>
                </div>
            </div>
        )
    }
}