import React from "react";
import Alert from "@material-tailwind/react/Alert";
import ParkRepository from "../../resources/park/ParkRepository";
import Button from "@material-tailwind/react/Button";
import { Link, Redirect } from "react-router-dom";

export default class ListParks extends React.Component {
    repository = new ParkRepository()

    constructor(props) {
        super(props)
        this.loadData()
    }

    headers = [
        {
            display: "Name",
            duck: false,
            key: "name"
        },
        {
            display: "Created By",
            duck: false,
            key: "createdBy"
        },
        {
            display: "Last time feed",
            duck: true,
            key: "feedTime",
            customize: x => new Date(x).toISOString()
        },
        {
            display: "Ducks quantity",
            duck: true,
            key: "quantity"
        },
        {
            display: "Ducks Feeded",
            duck: true,
            key: "fedQuantity"
        },
    ] 

    state = {
        parks: [],
        loading: true,
        error: false
    }

    loadData() {
        this.repository.getAll().then(x => {
            this.setState({parks: x, loading: false})
        }).catch(e => {
            this.setState({loading: false, error: true})
        })
    }

    render() {

        const parks = this.state.parks
        const headers = this.headers

        if(this.state.loading) return (<h3>Loading</h3>)
        else if (this.state.create) return (<Redirect to="/create"/>)
        else if(this.state.error) {
            return (<Alert color="red">Error during request to get all parks</Alert>)
        } else return (
            <div>
                <Button
                    color="lightBlue"
                    buttonType="filled"
                    size="regular"
                    rounded={true}
                    block={false}
                    iconOnly={false}
                    onClick={() => this.setState({create: true})}
                    ripple="dark">
                    New Park
                </Button>
                <table class="shadow-lg bg-white" style={{width: "100%"}}>
                    <thead>
                        <tr>
                            {headers.map(x => (<th class="bg-blue-100 border text-left px-8 py-4">{x.display}</th>))}
                            <th class="bg-blue-100 border text-left px-8 py-4"></th>
                        </tr>
                    </thead>
                    <tbody>
                        { parks.map(row => (
                            <tr>
                                {headers.map(x => (
                                    <td class="border px-8 py-4">{x.duck ? 
                                        (x.customize === undefined ? row.ducks[x.key] : x.customize(row.ducks[x.key])) : 
                                        (x.customize === undefined ? row[x.key] : x.customize(row[x.key]))}</td>
                                ))}
                                <td class="border px-8 py-4">
                                    <Link to={`/feed/${row.name}`}>
                                        <Button
                                            color="lightBlue"
                                            buttonType="link"
                                            size="sm"
                                            rounded={false}
                                            block={false}
                                            iconOnly={false}
                                            ripple="dark">
                                            Feed Ducks
                                        </Button>
                                    </Link>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}