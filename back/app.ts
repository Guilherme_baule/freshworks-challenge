require('dotenv').config()

import { container } from 'tsyringe';
import repositories from './src/application/config/modules/Repositories'
import { ParkRouter } from './src/application/web/routers/ParkRouter'
import DatabaseConfig from './src/application/config/database/DatabaseConfig'
import { UserRouter } from './src/application/web/routers/UserRouter'
import { authValidation } from './src/application/web/middlewarers/Auth'
import Bootstrap from './src/application/config/Bootstrap';
import cors from 'cors'

const bodyParser = require("body-parser");

const database = new DatabaseConfig()

var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser')
var logger = require('morgan');

var app = express();

app.use(cors());
app.options('*', cors());

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false })); 
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

database.connect()

repositories.inject()

const bootstrap = container.resolve(Bootstrap).init()

const parkRouter = container.resolve(ParkRouter).register()
const userRouter = container.resolve(UserRouter).register()

app.use('/user', userRouter)
app.use('/', authValidation , parkRouter);

export = app;
