import { Document, Model } from "mongoose";
import { Ducks } from "../../../../domain/entities/Ducks";

export interface IScheduler {
    scheduleId: string
    parkName: string
    username: string
    createdAt: Date
    hour: number
    minute: number
    timezone: string
    duck: Ducks
}

export interface ISchedulerDocument extends IScheduler, Document {}
export interface ISchedulerModel extends Model<ISchedulerDocument> {}
