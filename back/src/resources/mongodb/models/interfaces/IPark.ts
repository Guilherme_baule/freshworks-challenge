import { Document, Model } from "mongoose";

export interface IPark {
    parkId: String;
    name: String;
    createdBy: String;
}

export interface IParkDocument extends IPark, Document {}
export interface IParkModel extends Model<IParkDocument> {}
