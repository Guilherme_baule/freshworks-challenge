import { Document, Model } from "mongoose";

export interface IDuck {
    username: string,
    duckId: string
    feedTime: number
    quantity: number
    fedQuantity: Number
    parkId: String
}

export interface IDuckDocument extends IDuck, Document {}
export interface IDuckModel extends Model<IDuckDocument> {}