import { Document, Model } from "mongoose";

export interface IFood {
    type: String
    name: String
    quantity: number
    duckId: string
}

export interface IFoodDocument extends IFood, Document {}

export interface IFoodModel extends Model<IFoodDocument> {}