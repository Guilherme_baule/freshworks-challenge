import { Schema } from "mongoose";

export default new Schema({
    username: {
        type: String,
        required: true
    },
    duckId: {
        type: String,
        required: true
    },
    feedTime: {
        type: Number,
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    fedQuantity: {
        type: Number,
        required: true
    },
    parkId: {
        type: String,
        required: true
    }
})