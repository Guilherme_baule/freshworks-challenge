import { Schema } from "mongoose";

export default new Schema({
    parkId: String,
    name: String,
    createdBy: String
})