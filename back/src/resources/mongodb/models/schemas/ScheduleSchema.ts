import { Schema } from "mongoose";

export default new Schema({
    scheduleId: {
        type: String,
        required: true
    },
    parkName: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    hour: {
        type: Number,
        required: true
    },
    minute: {
        type: Number,
        required: true
    },
    timezone: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: true
    },
    duck: {
        type: Object,
        required: true
    }
})
