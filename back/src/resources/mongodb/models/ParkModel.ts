import mongoose from "mongoose";
import { IParkDocument } from "./interfaces/IPark";
import ParkSchema from "./schemas/ParkSchema";
import DuckSchema from "./schemas/DuckSchema";
import { IDuckDocument } from "./interfaces/IDuck";
import { IFoodDocument } from "./interfaces/IFood";
import FoodSchema from "./schemas/FoodSchema";
import { ISchedulerDocument } from "./interfaces/IScheduler";
import ScheduleSchema from "./schemas/ScheduleSchema";

export const duckModel = mongoose.model<IDuckDocument>('Duck', DuckSchema)
export const parkModel = mongoose.model<IParkDocument>('Park', ParkSchema)
export const foodModel = mongoose.model<IFoodDocument>('Food', FoodSchema)
export const scheduleModel = mongoose.model<ISchedulerDocument>('Scheduler', ScheduleSchema)