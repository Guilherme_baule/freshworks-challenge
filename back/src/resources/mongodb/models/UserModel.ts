import mongoose from "mongoose";
import { IUserDocument } from "./interfaces/IUser";
import UserSchema from "./schemas/UserSchema";

export const userModel = mongoose.model<IUserDocument>('Users', UserSchema)
