import { UserRepository } from "../../domain/repositories/UserRepository";
import { sha512 } from "../crypto/CryptoService";
import { userModel } from "./models/UserModel";
const { v4: uuidv4 } = require('uuid');

export default class UserRepositoryImpl implements UserRepository {

    async checkLogin(username: string, password: string): Promise<Boolean> {
        return await userModel.findOne({user: username, password: this.hashPassword(password)}) != null
    }

    async createUser(username: string, password: string): Promise<void> {
        const userExists = await this.exists(username)

        if(userExists) throw "User already exists"

        userModel.create({
            userId: uuidv4(),
            user: username,
            password: this.hashPassword(password),
            createdAt: Date.now()
        })
    }

    private hashPassword(password: string): string {
        return sha512(password, process.env.CUSTOMER_SECRET as string).passwordHash
    }

    private async exists(user: string): Promise<Boolean> {
        return await userModel.findOne({user}) != null
    }
}
