import { Park } from "../../domain/entities/Park";
import { ParkRepository } from "../../domain/repositories/ParkRepository";
import { duckModel, foodModel, parkModel, scheduleModel } from "./models/ParkModel";
const { v4: uuidv4 } = require('uuid');
import { Ducks } from "../../domain/entities/Ducks";
import { Food } from "../../domain/entities/Food";
import { IFood } from "./models/interfaces/IFood";
import Schedule from "../../domain/entities/Schedule";
import { mergeParkInfo } from "../mappers/ParkMapper";
import { IDuck } from "./models/interfaces/IDuck";

export class ParkRepositoryImpl implements ParkRepository {
    
    async getAll(): Promise<Park[]> {

        const parks = parkModel.find().exec()
        const ducks = await duckModel.aggregate([
            {"$group": {
                _id: "$parkId",
                parkId: { "$last": "$parkId"},
                username: {"$last": "$username"},
                duckId: {"$last": "$duckId"},
                feedTime: {"$last": "$feedTime"},
                quantity: {"$last": "$quantity"},
                fedQuantity: {"$last": "$fedQuantity"}
            }}
        ]).exec()

        const ducksIds = ducks.map(x => x.duckId);
        
        const food = await foodModel.find({duckId: {$in: ducksIds}}).exec()
        const foodMap = new Map<string, IFood>();
        const duckToPark = new Map<string, IFood>();

        ducks.forEach((duck: any) => {
            duckToPark.set(duck.parkId, duck);
        });

        food.forEach(x => {
            foodMap.set(x.duckId, x);
        });

        let parksResult = await parks;

        return parksResult.filter(park => {
            let id: string = `${park.parkId}`
            let duck = duckToPark.get(id)

            return duck !== null && foodMap.get(duckToPark.get(id)!.duckId) !== null
        }).map(park => {
            let id: string = `${park.parkId}`;
            let duck: IDuck = duckToPark.get(id);
            let food = foodMap.get(duckToPark.get(id)!.duckId)

            return mergeParkInfo(park, duck!, food!);
        });
    }

    async getAllSchedulers(): Promise<Schedule[]> {
        let jobs = await scheduleModel.find().exec()
        
        return jobs.map(v => {
            return new Schedule(
                v.username,
                v.parkName,
                v.hour, 
                v.minute, 
                v.timezone,
                new Ducks(
                    v.duck.feedTime,
                    new Food(
                        v.duck.food.type,
                        v.duck.food.name,
                        v.duck.quantity
                    ),
                    v.duck.quantity,
                    v.duck.fedQuantity
                ),
                v.scheduleId
            )
        })
    }

    async scheduleFeed(username: string, name: String, schedule: Schedule): Promise<string> {
        
        if (!await this.existsByName(name)) throw "Park doesnt exists";

        let id = uuidv4()
        try {
            await scheduleModel.create({
                scheduleId: id,
                parkName: name,
                username,
                createdAt: Date.now(),
                hour: schedule.hour,
                minute: schedule.minute,
                timezone: schedule.timezone,
                duck: schedule.duck
            })
            return id
        } catch(e) {
            console.error(e)
            throw "Error during creation of scheduler"
        }
    }

    async getHistory(name: String): Promise<Ducks[]> {
        const park = await this.get(name)
        
        const ducks = await duckModel.find({parkId: park!.id}).sort({feedTime: -1}).exec()
        const duckIds = ducks.map(x => x.duckId);

        const food = await foodModel.find({duckId: {$in: duckIds}})
        const foodMap = new Map<string, IFood>();

        food.forEach(x => {
            foodMap.set(x.duckId, x)
        })

        return ducks.map(duck => {
            let currentFood = foodMap.get(duck.duckId)
            return new Ducks(
                duck.feedTime,
                new Food(
                    currentFood!.type,
                    currentFood!.name,
                    currentFood!.quantity
                ),
                duck.quantity,
                duck.fedQuantity
            )
        })        
    }

    async feed(username: string, name: String, duck: Ducks): Promise<Park> {
        const park = await this.get(name)
        this.addDuck(username, park.id, duck)
        return await this.get(name)
    }

    async create(userId: string, parkName: String, duck: Ducks) {
        if (await this.existsByName(parkName)) throw "Park with this name already exists";
        
        let parkId = uuidv4()
        
        this.addDuck(userId, parkId, duck)

        await parkModel.create({
            parkId: parkId,
            createdBy: userId,
            name: parkName
        })
    }

    async get(name: String): Promise<Park> { 
        const park = await parkModel.findOne({name})
        const duckResult = await duckModel.find({parkId: park!.parkId}).sort({feedTime: -1}).exec()

        const duck = duckResult[0]
        const food = await foodModel.findOne({duckId: duck.duckId})

        if(park && duck && food) return mergeParkInfo(park, duck, food)
        else throw "Not Found";
    }
    
    delete(id: String): boolean {
        return true
    }

    private async addDuck(username: string, parkId: String, duck: Ducks): Promise<void> {
        let duckId = uuidv4()
        await duckModel.create({
            username,
            duckId: duckId,
            feedTime: Date.now(),
            quantity: duck.quantity,
            fedQuantity: duck.fedQuantity,
            parkId: parkId
        })

        await foodModel.create({
            duckId: duckId,
            type: duck.food.type,
            name: duck.food.name,
            quantity: duck.food.quantity
        })
    }

    private async existsByName(name: String): Promise<Boolean> {
        return await parkModel.findOne({name}) !== null
    }
}
