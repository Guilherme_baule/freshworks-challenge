import { Ducks } from "../../domain/entities/Ducks";
import { Food } from "../../domain/entities/Food";
import { Park } from "../../domain/entities/Park";
import { IDuck } from "../mongodb/models/interfaces/IDuck";
import { IFood } from "../mongodb/models/interfaces/IFood";
import { IPark } from "../mongodb/models/interfaces/IPark";

export = {
    mergeParkInfo(park: IPark, duck: IDuck, food: IFood): Park {
        return new Park(
            park!.parkId,
            park!.name,
            park!.createdBy,
            new Ducks(
                duck.feedTime,
                new Food(
                    food.type,
                    food.name,
                    food.quantity
                ),
                duck.quantity,
                duck.fedQuantity
            )
        )
    }
}