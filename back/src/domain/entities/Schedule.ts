import { Ducks } from "./Ducks";

export default class Schedule {
    constructor(
        public username: string,
        public parkName: string,
        public hour: number,
        public minute: number,
        public timezone: string = 'Etc/UTC',
        public duck: Ducks,
        public id?: string
    ) {}
}
