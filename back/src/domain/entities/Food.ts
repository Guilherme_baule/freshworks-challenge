export class Food {
    constructor (
        public type: String,
        public name: String,
        public quantity: Number,
    ) {}
}
