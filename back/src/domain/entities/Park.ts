import { Ducks } from "./Ducks";

export class Park {
    constructor (
        public id: String,
        public name: String,
        public createdBy: String,
        public ducks: Ducks
    ) {}
}
