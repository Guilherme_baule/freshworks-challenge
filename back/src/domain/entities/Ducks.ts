import { Food } from "./Food";

export class Ducks {
    constructor(
        public feedTime: number,
        public food: Food,
        public quantity: Number,
        public fedQuantity: Number
    ) {}
}
