import { inject, injectable } from "tsyringe";
import { UserRepository } from "../repositories/UserRepository";

@injectable()
export default class UserService {
    
    constructor(@inject("UserRepository") private userRepository: UserRepository) {}

    async createUser(username: string, password: string): Promise<void> {
        return this.userRepository.createUser(username, password)
    }

    async login(username: string, password: string): Promise<void> {
        if (!await this.userRepository.checkLogin(username, password)) throw "Unauthorized"
    }
}