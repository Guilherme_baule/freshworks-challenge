import { inject, injectable } from "tsyringe";
import { Ducks } from "../entities/Ducks";
import { Park } from "../entities/Park";
import Schedule from "../entities/Schedule";
import { ParkRepository } from "../repositories/ParkRepository";

@injectable()
export default class ParkService {
    constructor(
        @inject("ParkRepository") private parkRepository: ParkRepository
    ) {}

    async create(userId: string, parkName: string, duck: Ducks) {
        return this.parkRepository.create(userId, parkName, duck).catch(err => {
            console.error(err)
            throw err;
        })
    }

    async getAll(): Promise<Park[]> {
        return await this.parkRepository.getAll()
    }

    async getPark(name: string): Promise<Park> {
        return await this.parkRepository.get(name)
    }

    async feed(username: string, name: String, ducks: Ducks) {
        return await this.parkRepository.feed(username, name, ducks)
    }

    async scheduleFeed(username: string, name: string, schedule: Schedule): Promise<string> {
        return this.parkRepository.scheduleFeed(username, name, schedule).catch(err => {
            console.error(err)
            throw err;
        })
    }

    async history(name: string): Promise<Array<Ducks>> {
        return await this.parkRepository.getHistory(name)
    }

    delPark(id: string) {
        this.parkRepository.delete(id)
    }

    async getAllScheduled(): Promise<Schedule[]> {
        return this.parkRepository.getAllSchedulers()
    }
}
