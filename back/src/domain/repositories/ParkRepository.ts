import { Ducks } from "../entities/Ducks";
import { Park } from "../entities/Park";
import Schedule from "../entities/Schedule";

export interface ParkRepository {
    create(userId: string, parkName: String, duck: Ducks): Promise<void>
    get(name: string): Promise<Park>
    getAll(): Promise<Park[]>
    delete(id: string): boolean
    feed(username: string, name: String, duck: Ducks): Promise<Park>
    getHistory(name: string): Promise<Ducks[]>
    scheduleFeed(username: string, name: string, schedule: Schedule): Promise<string>
    getAllSchedulers(): Promise<Array<Schedule>>
}
