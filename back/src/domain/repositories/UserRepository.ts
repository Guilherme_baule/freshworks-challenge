export interface UserRepository {
    createUser(username: string, password: string): Promise<void>
    checkLogin(username: string, password: string): Promise<Boolean>
}