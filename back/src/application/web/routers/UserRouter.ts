import express, { Request, Router } from 'express';
import { autoInjectable } from "tsyringe";
import UserController from '../controllers/UserController'
import RegisterRequest from '../entities/RegisterRequest'

const router = express.Router();

@autoInjectable()
export class UserRouter {
    constructor(private userController: UserController) {}

    register(): Router {
        router.post('/register', ({ body }, res: any) => {
            const request = body as RegisterRequest
            this.userController.create(request.username, request.password).then(() => {
                res.status(204).end()
            }).catch(error => {
                res.status(400).json({error})
            })
        })

        router.post('/login', ({ body }, res: any) => {
            const request = body as RegisterRequest
            this.userController.login(request.username, request.password).then(r => {
                res.status(200).json(r)
            }).catch(error => {
                res.status(401).json({error})
            })
        })

        return router
    }
}