import express, { Request, Router } from 'express';
import { autoInjectable } from "tsyringe";
import { Ducks } from '../../../domain/entities/Ducks';
import Schedule from '../../../domain/entities/Schedule';
import ParkController from "../controllers/ParkController";
import ParkRequest from '../entities/ParkRequest';
import ScheduleFeedRequest from '../entities/ScheduleFeedRequest';
const router = express.Router();

@autoInjectable()
export class ParkRouter {
    constructor(private parkController: ParkController) {}

    register(): Router {
        router.post('/create-park', (req: any, res: any) => {
            this.parkController.create(
                req.authInfo!.username, 
                req.body as ParkRequest
            ).then(() => {
                res.status(204).end()
            }).catch(error => {
                res.status(500).json({ error })
            })
        });

        router.put('/feed/:parkName', async (req: any, res: any) => {
            this.parkController.feed(
                req.authInfo!.username, 
                req.params.parkName, 
                req.body as Ducks
            ).then(() => { res.status(204).end() }).catch(error => {
                res.status(500).json({ error })
            })
        })

        router.put('/feed/:parkName/schedule', async (req: any, res: any) => {
            let body = req.body as ScheduleFeedRequest
            let schedule = new Schedule(
                req.authInfo!.username,
                req.params.parkName, 
                body.hour, 
                body.minute, 
                body.timezone, 
                body.duck
            )

            this.parkController.feedSchedule(schedule).then(() => {
                res.status(204).end()
            }).catch(error => {
                res.status(500).json({ error })
            })
        });

        router.get('/', async (req: any, res: any, next: any) => {
            res.json(await this.parkController.getAll())
        });

        router.get('/:name', async (req: any, res: any, next: any) => {
            res.json(await this.parkController.get(req.params.name))
        });

        router.get('/:name/history', async (req: any, res: any, next: any) => {
            res.json(await this.parkController.history(req.params.name))
        });

        return router
    }
}
