import { autoInjectable } from "tsyringe";
import { Ducks } from "../../../domain/entities/Ducks";
import { Park } from "../../../domain/entities/Park";
import Schedule from "../../../domain/entities/Schedule";
import ParkService from "../../../domain/services/ParkService";
import scheduleJob from "../../config/scheduler/SchedulerConfig";
import { DuckResponse } from "../entities/DuckResponse";
import ParkRequest from "../entities/ParkRequest";

@autoInjectable()
export default class ParkController {    
    constructor(private parkService: ParkService) { }

    async create(username: string, request: ParkRequest) {
        return this.parkService.create(username, request.name, request.ducks).catch(e => {throw e})
    }

    async get(name: string): Promise<Park> {
        return await this.parkService.getPark(name)
    }

    async getAll(): Promise<Park[]> {
        return await this.parkService.getAll()
    }

    async history(name: string): Promise<Array<DuckResponse>> {
        return await (await this.parkService.history(name)).map(duck => {
            return new DuckResponse(new Date(duck.feedTime), duck.food, duck.quantity, duck.fedQuantity);
        })
    }

    async feed(username: string, name: string, request: Ducks) {
        return await this.parkService.feed(username, name, request)
    }

    async feedSchedule(config: Schedule) {
        try {
            let id = await this.parkService.scheduleFeed(config.username, config.parkName, config)
            scheduleJob(id, config, async () => { await this.parkService.feed(config.username, config.parkName, config.duck) })
        } catch(e) {
            throw e
        }
    }
}
