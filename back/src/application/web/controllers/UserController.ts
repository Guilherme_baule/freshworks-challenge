import { autoInjectable } from "tsyringe";
import UserService from "../../../domain/services/UserService";
import { LoginResponse } from "../entities/LoginResponse";
import jwt from 'jsonwebtoken'

@autoInjectable()
export default class UserController {    
    constructor(private userService: UserService) { }

    async create(username: string, password: string): Promise<void> {
        return await this.userService.createUser(username, password)
    }

    async login(username: string, password: string): Promise<LoginResponse> {
        return await this.userService.login(username, password).then(() => {
            return new LoginResponse(
                jwt.sign({username}, process.env.JWT_SECRET as string, { expiresIn: process.env.JWT_DURATION }))
        }).catch(() => {
            throw "Unauthorized"
        })
    }
}
