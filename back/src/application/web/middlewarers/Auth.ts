import { Request } from "express-serve-static-core"
import jwt from 'jsonwebtoken'

export = {
    authValidation: async (req: Request, res : any, next: any) => {
        if (!req.headers.authorization) return res.status(401).json({error: "Unauthorized"})

        let authToken = req.headers.authorization.replace("Bearer ", "");
        try {
            let jwtResp = await jwt.verify(authToken, process.env.JWT_SECRET as string)
            req.authInfo = jwtResp
            next()
        } catch(e) {
            console.log(e)
            return res.status(401).json({error: "Unauthorized"})
        }
    }
}