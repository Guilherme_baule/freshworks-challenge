import { Ducks } from "../../../domain/entities/Ducks";

export default class ParkRequest {
    constructor(
        public name: string,
        public ducks: Ducks
    ) {}
}
