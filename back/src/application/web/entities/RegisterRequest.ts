export default class RegisterRequest {
    constructor(
        public username: string,
        public password: string
    ) {}
}
