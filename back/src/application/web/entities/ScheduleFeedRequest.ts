import { Ducks } from "../../../domain/entities/Ducks";

export default class ScheduleFeedRequest {
    constructor(
        public duck: Ducks,
        public hour: number,
        public timezone: string,
        public minute: number
    ) {}
}
