import { Ducks } from "../../../domain/entities/Ducks";
import { Food } from "../../../domain/entities/Food";

export class DuckResponse {
    constructor(
        public feedTime: Date,
        public food: Food,
        public quantity: Number,
        public fedQuantity: Number,
    ) {}
}
