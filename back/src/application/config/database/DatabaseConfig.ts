import mongoose from "mongoose";

export default class DatabaseConfig {
    
    connect() {
        mongoose.connect('mongodb://localhost:27017/parks', {
            useCreateIndex: true,
            useNewUrlParser: true,
            useUnifiedTopology: true
        }, () => {
            console.log("Database connected")
        })
    }

    disconnect() {
        mongoose.disconnect()
    }
}
