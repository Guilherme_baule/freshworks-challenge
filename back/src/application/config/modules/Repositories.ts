import { container } from 'tsyringe'
import { ParkRepository } from '../../../domain/repositories/ParkRepository'
import { UserRepository } from '../../../domain/repositories/UserRepository'
import { ParkRepositoryImpl } from '../../../resources/mongodb/ParkRepositoryImpl'
import UserRepositoryImpl from '../../../resources/mongodb/UserRepositoryImpl'

export = {
    inject: () => { 
        container.register<ParkRepository>("ParkRepository", { useClass: ParkRepositoryImpl })
        container.register<UserRepository>("UserRepository", { useClass: UserRepositoryImpl }) 
    }
}
