import { autoInjectable } from "tsyringe";
import ParkService from "../../domain/services/ParkService";
import ParkController from "../web/controllers/ParkController";
import scheduleJob from "./scheduler/SchedulerConfig";

@autoInjectable()
export default class Bootstrap {

    constructor(private parkService: ParkService) {}

    async init() {
        let jobs = await this.parkService.getAllScheduled()
        console.log(`Loaded ${jobs.length} jobs`)
        jobs.forEach(job => {
            console.log(`Start scheduled id: ${job.id} park: ${job.parkName} to run every day at ${job.hour}:${job.minute} tz: ${job.timezone}`)
            scheduleJob(job.id!, job, async () => { await this.parkService.feed(job.username, job.parkName, job.duck) })
        })
    }

}