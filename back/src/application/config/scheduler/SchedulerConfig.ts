import Schedule from "../../../domain/entities/Schedule"
import scheduler from 'node-schedule'

let schedulerJobs = new Map<string, any>()

const scheduleJob = (id: string, config: Schedule, job: () => Promise<void>) => {
    const rule = new scheduler.RecurrenceRule()
    rule.hour = config.hour
    rule.minute = config.minute
    rule.tz = config.timezone

    schedulerJobs.set(id, scheduler.scheduleJob(rule, async () => {
        console.info(`running scheduled id:${id}`)
        job().then(() => {
            console.log(`scheduler id: ${id} executed successfully`)
        }).catch(error => {
            console.error(`scheduler id: ${id} error: ${error}`)
        })
    }))
}

export = scheduleJob